#!/usr/bin/env python

class Base1:
    pass

class Base2:
    pass

class MultiDerived(Base1, Base2):
    pass
